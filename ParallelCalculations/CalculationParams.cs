﻿namespace ParallelCalculations
{
    public class CalculationParams
    {
        public List<int> elements { get; set; }
        public int result { get; set; }
        public bool createSubThreads { get; set; }

        public CalculationParams(List<int> _elements, int _result, bool _createSubThreads = false)
        {
            elements = _elements;
            result = _result;
            createSubThreads = _createSubThreads;
        }
    }
}
