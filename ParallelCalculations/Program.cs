﻿using ParallelCalculations;
using System;
using System.Diagnostics;

TestListCalculator(100000);
TestListCalculator(1000000);
TestListCalculator(10000000);

void TestListCalculator(int ElementsNum)
{
    ListCalculator calculator = new ListCalculator(ElementsNum);
    CalculateTotal(calculator, "async");
    CalculateTotal(calculator, "sync");
    CalculateTotal(calculator, "linq");
}

void CalculateTotal(ListCalculator Calculator, string CalculateType)
{
    Stopwatch stopWatch = new Stopwatch();
    stopWatch.Start();

    int total = 0;
    var calcParams = new CalculationParams(Calculator._elements, total);

    switch (CalculateType)
    {
        case "async": 
            total = ListCalculator.CalculateTotal(Calculator._elements);
            break;
        case "sync":          
            ListCalculator.CalculateTotalSync(calcParams);
            total = calcParams.result;
            break;
        case "linq":
            total = ListCalculator.CalculateTotalLinq(Calculator._elements);
            break;
    }

    stopWatch.Stop();

    Console.WriteLine($"{Calculator._elementsNum} elements total = {total} with {CalculateType},  in {stopWatch.ElapsedMilliseconds} ms");
}