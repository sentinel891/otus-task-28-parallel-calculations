﻿namespace ParallelCalculations
{
    class ListCalculator
    {
        public int _elementsNum { get; set; }
        public List<int> _elements { get; set; }
        public ListCalculator(int elementsNum)
        {
            _elementsNum = elementsNum;
            _elements = new List<int>();

            Random rnd = new Random();

            for (int i = 0; i < elementsNum; i++)
            {
                _elements.Add(rnd.Next(1,100));
            }
        }

        public static int CalculateTotal(List<int> elements)
        {
            int total = 0;
            foreach (int i in elements) total += i;
            return total;
        }

        public static void CalculateTotalSync(object? calcParamsObj)
        {
            int divideStep = 100;

            var calcParams = (CalculationParams)calcParamsObj;
            List<int> elements = calcParams.elements;

            if (calcParams.createSubThreads == false)
            {
                calcParams.result = CalculateTotal(elements);
            }
            else
            {
                int parallelElementsCount = elements.Count / divideStep;
                var elementsParallel = new CalculationParams[divideStep];

                for (int i = 0; i < divideStep; i++)
                {
                    int subResult = 0;
                    elementsParallel[i] = new CalculationParams(elements.GetRange(i*parallelElementsCount, parallelElementsCount), subResult, false);
                    Thread thread = new Thread(CalculateTotalSync);
                    thread.Start(elementsParallel[i]);
                    thread.Join();
                }

                List<int> resultList = new List<int>();
                foreach (CalculationParams calcParam in elementsParallel) 
                {
                    resultList.Add(calcParam.result);
                }

                calcParams.result = CalculateTotal(resultList);
            };
        }

        public static int CalculateTotalLinq(List<int> elements)
        {
            int divideStep = 100;

            int parallelElementsCount = elements.Count / divideStep;
            var elementsParallel = new List<int>[divideStep];

            for (int i = 0; i < divideStep; i++)
            {
                elementsParallel[i] = elements.GetRange(i * parallelElementsCount, parallelElementsCount);
            }

            List<int> resultList = new List<int>();
            (from n in elementsParallel.AsParallel() select CalculateTotal(n)).ForAll(n => resultList.Add(n));

            return CalculateTotal(resultList);
        }
    }
}
